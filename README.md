# Déclaration accessibilité RGAA

Recensement des déclarations de conformité du RGAA (Référentiel Général d'Amélioration de l'Accessibilité)

## Méthodologie

### Périmètre

La liste a vocation à recenser les déclarations de conformité issues de la sphère publique en général.
Les déclarations d'acteurs privés peuvent aussi être incluses à titre de comparaison.

### Conformité

La conformité notée ici est la conformité **affichée** dans la déclaration. Seuls les résultats affichés de
*non-conformité* ou *conformité partielle* ou *conformité totale* sont repris ici.

Pour commencer, même si une interprétation humaine permet de le déduire, le niveau de conformité n'est pas reporté
dans le liste (par exemple en affichant un pourcentage sans pour autant écrire le niveau de conformité).

### Dernière mise à jour

La *dernière mise à jour* concerne la ligne du tableau (et non la déclaration de conformité du site mentionné)

### Prestataire

Certaines déclarations de conformités affichent le prestataire l'ayant réalisée. Si c'est le cas, ce dernier est
mentionné dans le tableau.
